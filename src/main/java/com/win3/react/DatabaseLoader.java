/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.win3.react;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLoader implements CommandLineRunner {

    private final EmployeeRepository repository;

    @Autowired
    public DatabaseLoader(EmployeeRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... strings) throws Exception {
        this.repository.save(new Employee("Klaus", "Jenny", "CEO"));
        this.repository.save(new Employee("Franz K. von", "Meyenburg", "Director"));
        this.repository.save(new Employee("Olivier Robert", "Steimer", "HR Manager"));
        this.repository.save(new Employee("Beat Martin ", "Fenner", "Recruiter"));
        this.repository.save(new Employee("Luc", "Argand", "Marketing Manager"));
        this.repository.save(new Employee("John", "Johnson", "Trainer"));
        this.repository.save(new Employee("Michael", "Brown", "Trainer"));
        this.repository.save(new Employee("Mark", "Gonzalez", "Employee"));
        this.repository.save(new Employee("Paul", "Smith", "Employee"));
        this.repository.save(new Employee("John", "Lee", "Employee"));
        this.repository.save(new Employee("James", "Williams", "Employee"));
        this.repository.save(new Employee("Maria", "Garcia", "Employee"));
    }
}