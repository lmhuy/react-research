import React, {Component, PropTypes} from 'react';
import {itemsFetchData, updateEmployee} from '../actions/items'
import {connect} from 'react-redux';

class EmployeeDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            firstName: this.props.firstName,
            lastName: this.props.lastName,
            description: this.props.description
        };

        this.onButtonClick = this.onButtonClick.bind(this);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleDescChange = this.handleDescChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {

        console.log('EmployeeDetail render');

        return (
            <div>

                <form className="form-group employee-edit-form" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>First Name:</label>
                        <input className="form-control" type="text" value={this.state.firstName}
                               onChange={this.handleFirstNameChange}/>
                    </div>
                    <div className="form-group">
                        <label>Last Name:</label>
                        <input className="form-control" type="text" value={this.state.lastName}
                               onChange={this.handleLastNameChange}/>
                    </div>
                    <div className="form-group">
                        <label>Description:</label>
                        <input className="form-control" type="text" value={this.state.firstName}
                               onChange={this.handleDescChange}/>
                    </div>
                    <div className="employee-edit-form-button">
                        <button type="button" className="btn btn-default" onClick={this.onButtonClick}>Back</button>
                        <button type="submit" className="btn btn-default save">Save</button>
                    </div>

                </form>
            </div>
        );
    }

    onButtonClick() {
        this.props.itemsFetchData('http://localhost:8080/api/employees');
    }

    handleFirstNameChange(event) {
        this.setState({firstName: event.target.value});
    }

    handleLastNameChange(event) {
        this.setState({lastName: event.target.value});
    }

    handleDescChange(event) {
        this.setState({description: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();

        console.log('state' + this.state);

        this.props.updateEmployee('http://localhost:8080/api/employees/' + this.props.id, this.state);
    }

}

EmployeeDetail.propTypes = {
    itemsFetchData: PropTypes.func.isRequired,
    updateEmployee: PropTypes.func.isRequired,
    hasErrored: PropTypes.bool.isRequired,
    employee: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
    return {
        employee: state.employee,
        hasErrored: state.itemsHasErrored
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        itemsFetchData: (url) => dispatch(itemsFetchData(url)),
        updateEmployee: (url, employee) => dispatch(updateEmployee(url, employee))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeDetail);
