import React, { Component } from 'react';
import { goToEmployeePage } from '../actions/items'

export default class EmployeeRow extends Component {
    constructor(props, store) {
		super(props);

        this.store = store.store;
		this.onNameClick = this.onNameClick.bind(this);
	}
    
    render() {
        return (
            <tr>
                <th scope="row">{this.props.id}</th>
                <td onClick={this.onNameClick}>{this.props.firstName}</td>
                <td>{this.props.lastName}</td>
                <td>{this.props.description}</td>
            </tr>
        );
    }

    onNameClick() {
        this.store.dispatch(goToEmployeePage(this.props));
    }
}

EmployeeRow.contextTypes = {
  store: React.PropTypes.object
};