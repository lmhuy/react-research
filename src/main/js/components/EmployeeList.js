import React, { Component } from 'react';
import EmployeeRow from './EmployeeRow';

import { BarChart } from 'react-d3';

export default class EmployeeList extends Component {
    render() {
        let employeeRows = this.props.employees.map((employee) => (
            <EmployeeRow key={employee.id}
                         {...employee} />
        ));

        return (
            <div className="container employee-list-form">
                <table className="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Desciptions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {employeeRows}
                    </tbody>
                </table>
            </div>
        );
    }
}