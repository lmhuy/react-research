import React, {Component} from "react";
import {PieChart} from "react-d3";

export default class D3React extends Component {
    render() {

        let desciptions = [
            'CEO',
            'Director',
            'HR Manager',
            'Recruiter',
            'Marketing Manager',
            'Trainer',
            'Employee'
        ];

        let employeeData = [];

        for (let desciption of desciptions) {
            let desCount = 0;

            for (let employee of this.props.employees) {
                if (desciption == employee.description) {
                    console.log(desciption);
                    desCount++;
                }
            }

            let percent = Math.round(desCount * 10 / this.props.employees.length * 100) / 10;

            employeeData.push({label: desciption, value: percent/*desCount * 100 / this.props.employees.length*/});
        }

        console.log(employeeData);

        return (
            <PieChart
                data={employeeData}
                width={400}
                height={400}
                radius={100}
                innerRadius={20}
                sectorBorderColor="white"
                title="Pie Chart"
            />
        );
    }
}

