import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { itemsFetchData } from '../actions/items';
import EmployeeList from './EmployeeList'
import EmployeeDetail from './EmployeeDetail'
import D3React from './D3React';

class EmployeeContainer extends Component {
    componentDidMount() {
        this.props.fetchData('http://localhost:8080/api/employees');
    }

    render() {

        console.log('EmployeeContainer render');

        if (this.props.hasErrored) {
            return <p>Sorry! There was an error loading the items</p>;
        }

        if (this.props.isLoading) {
            return <p>Loading…</p>;
        }

        if (this.props.employee.firstName) {
            return <EmployeeDetail {...this.props.employee} />;
        } else {
            return (
                <div>
                    <EmployeeList employees={this.props.items}/>
                    <D3React employees={this.props.items}/>
                </div>);
        }
    }
}

EmployeeContainer.propTypes = {
    fetchData: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired,
    hasErrored: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    employee: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
    return {
        items: state.items,
        hasErrored: state.itemsHasErrored,
        isLoading: state.itemsIsLoading,
        employee: state.employee
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(itemsFetchData(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeContainer);
