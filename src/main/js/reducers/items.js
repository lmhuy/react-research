export function itemsHasErrored(state = false, action) {
    switch (action.type) {
        case 'ITEMS_HAS_ERRORED':
            return action.hasErrored;

        default:
            return state;
    }
}

export function itemsIsLoading(state = false, action) {
    switch (action.type) {
        case 'ITEMS_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function items(state = [], action) {
    switch (action.type) {
        case 'ITEMS_FETCH_DATA_SUCCESS':
            return action.items;

        default:
            return state;
    }
}

export function employee(state = {}, action) {
    switch (action.type) {
        case 'GO_TO_DETAIL_PAGE':
        case 'UPDATE_SUCCESS':
            return action.employee;
        case 'RESET_EMPLOYEE':
            return {};
        default:
            return state;
    }
}

export function updateError(state = {}, action) {
    switch (action.type) {
        case 'UPDATE_ERROR':
            return action.updateError;
        default:
            return state;
    }
}
