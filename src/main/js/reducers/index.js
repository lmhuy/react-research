import { combineReducers } from 'redux';
import { items, itemsHasErrored, itemsIsLoading, employee, updateError } from './items';

export default combineReducers({
    items,
    itemsHasErrored,
    itemsIsLoading,
    employee,
    updateError
});
