import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';

import EmployeeContainer from './components/EmployeeContainer';

const store = configureStore();

render(
    <Provider store={store}>
        <EmployeeContainer />
    </Provider>,
    document.getElementById('react')
);
