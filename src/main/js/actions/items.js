export function itemsHasErrored(bool) {
    return {
        type: 'ITEMS_HAS_ERRORED',
        hasErrored: bool
    };
}

export function itemsIsLoading(bool) {
    return {
        type: 'ITEMS_IS_LOADING',
        isLoading: bool
    };
}

export function itemsFetchDataSuccess(items) {
    return {
        type: 'ITEMS_FETCH_DATA_SUCCESS',
        items
    };
}

export function resetEmployee() {
    return {
        type: 'RESET_EMPLOYEE'
    };
}

export function itemsFetchData(url) {
    return (dispatch) => {

        dispatch(resetEmployee({}));
        dispatch(itemsIsLoading(true));

        fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(itemsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(itemsFetchDataSuccess(data._embedded.employees))
            })
            .catch(() => dispatch(itemsHasErrored(true)));
    };
}

export function goToEmployeePage(employee){
    return {
        type: 'GO_TO_DETAIL_PAGE',
        employee
    }
}

export function updateSuccess(employee) {
    return {
        type: 'UPDATE_SUCCESS',
        employee
    };
}

export function updateError(bool) {
    return {
        type: 'UPDATE_ERROR',
        hasErrored: bool
    };
}

export function updateEmployee(url, employee) {
    return (dispatch) => {
        fetch(url, {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "firstName": employee.firstName,
                "lastName": employee.lastName,
                "description": employee.description
            })
        }).then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.json();
        }).then((employee) => {
            console.log('response');
            console.log(employee);
            dispatch(updateSuccess(employee))
        }).catch(() => dispatch(updateError(true)));
    };
}